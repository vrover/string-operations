
public class Question10 {
	private String s;

	public Question10(String s) {
		super();
		this.s = s.replaceAll("\\s", "");
	}

	public String getS() {
		return s;
	}
	
	public int calculateAge(String s) {
		//calculating the age
		return s.length()+Character.toUpperCase(s.charAt(s.length()-1))-64;
		
	}
	public static void main(String[] args) {
		Question10 obj=new Question10("ABAB");
		
		//printing the result
		System.out.println(obj.calculateAge(obj.getS()));
	}
	

}
