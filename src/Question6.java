
public class Question6 {
	public String rotateString(String s) 
	{
		if(s.length()==1) return(s);
		else if(s.length()%2==0) {
			return(s.charAt(s.length()-1)+s.substring(0, s.length()-1));
		}
		else  {
			return(s.substring(1, s.length())+s.charAt(0));
		}
	}
public static void main(String[] args) {
	System.out.print(new Question6().rotateString("rsvgh"));
	
}
}
