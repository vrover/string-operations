//functional interface to carry out the operation
@FunctionalInterface
interface Operation2{
	//function with the logic
	void count(String s);
	
}
public class Question2 {
private String T;
public Question2(String s) {
	this.T = s;
}

	public String gets() {
	return T;
}
	public static void main(String[] args) {
		Question2 obj=new Question2("VarD");
		
		//defining lambda expression
		Operation2 ob= (Operation2)((String s)->{ 
			StringBuilder lower=new StringBuilder();
			StringBuilder upper=new StringBuilder();
			for(int i=0;i<s.length();i++) {if(s.charAt(i)>='A' && s.charAt(i)<='Z') upper.append(s.charAt(i));
			                          else  lower.append(s.charAt(i));
			                              }
			if(upper.length()!=lower.length()) {
				System.out.println("lowercase : "+lower);
				System.out.println("uppercase : "+upper);
				
			}
			else System.out.print("Equally Distributed");
         });
		
		// calling function in the interface
		ob.count(obj.gets());


	}


}
