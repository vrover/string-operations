import java.util.HashMap;

public class Question8 {
private String s;

public String getS() {
	return s;
}

public Question8(String s) {
	super();
	//replace white spaces from the string
	this.s = s.replaceAll("\\s", "");
}
public void occurranceMaxAndMin(String s) {
	//creating a map with characters as keys and frequency of occurrance as value
	HashMap<Character,Integer> frequencyMap=new HashMap<Character,Integer>();
	int max_freq=1, min_freq=1;
	char max_freq_char=s.charAt(0),min_freq_char=s.charAt(0);
	
	for(int i=0;i<s.length();i++) {
		if(!frequencyMap.containsKey(s.charAt(i))) {
			frequencyMap.put(s.charAt(i), 1);
			if(min_freq>1) {min_freq=1; min_freq_char=s.charAt(i);}
			
		}
		else {
			int new_freq=frequencyMap.get(s.charAt(i))+1;
			frequencyMap.put(s.charAt(i), new_freq);
			if(new_freq>max_freq) {max_freq=new_freq; max_freq_char=s.charAt(i);}
			if(s.charAt(i)==min_freq_char) {min_freq++;}
			if(new_freq<min_freq){min_freq=new_freq; min_freq_char=s.charAt(i);}
			
		}
		
	}
	System.out.println(max_freq_char+" : "+max_freq);
	System.out.println(min_freq_char+" : "+min_freq);
	
}
public static void main(String[] args) {
	Question8 obj=new Question8("varshant");
	
	//calling the function
	obj.occurranceMaxAndMin(obj.getS());
	
}
}
