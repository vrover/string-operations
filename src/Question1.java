//functional interface to carry out the operation
@FunctionalInterface
interface Operation1{
	// function that holds the logic
	String reverse(String s, String S1);
	
}
public class Question1 {
private String S,S1;

	public String getS() {
	return S;
}

public String getS1() {
	return S1;
}

	public Question1(String S, String S1) {
	this.S = S;
	this.S1 = S1;
}

	public static void main(String[] args) {
		
	Question1 obj=new Question1("Wipro is","Wipro is");
	
	// defining lambda expression
	Operation1 ob= (Operation1)((String s,String s1)->{ if(s.equals(s1)) return (new StringBuilder(s).reverse()).toString();
	                                      else return "Reverse Not Supported";
	                                       });
	// executing expression and printing results
	System.out.print(ob.reverse(obj.getS(),obj.getS1()));
	}
    
}
