
public class Question7 {
	private String S1,S2;

	public Question7(String s1, String s2) {
		super();
		S1 = s1.replaceAll("\\s", "");
		S2 = s2.replaceAll("\\s", "");
	}

	public String getS1() {
		return S1;
	}

	public String getS2() {
		return S2;
	}
	public String merge(String s1,String s2) {
		int i=0;
		StringBuilder result=new StringBuilder();
		
		while(i!=s1.length() && i!=s2.length()) {
			result.append(s1.charAt(i));
			result.append(s2.charAt(i));
			i++;
		}
		if(i==s1.length()) {result.append(s2.substring(i, s2.length()));}
		else {result.append(s1.substring(i, s1.length()));}
		return result.toString();
	}
	public static void main(String[] args) {
		Question7 obj=new Question7("A","DEF");
		String finalString=obj.merge(obj.getS1(),obj.getS2());
		System.out.println(finalString);
		
	}

}
