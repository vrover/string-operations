
//functional interface to carry out the operation
@FunctionalInterface
interface Operation5{
	// function that holds the logic
	StringBuilder toggle(String s);
	
}
public class Question5 {
private String s;

public Question5(String s) {
	this.s = s;
}

public String getS() {
	return s;
}
public static void main(String[] args) {
	Question5 obj=new Question5("lETS havE fUN");
	
	// defining lambda expression
	Operation5 ob=(Operation5)((String s)->{
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i)==' ')sb.append(" ");
			else if(Character.isUpperCase(s.charAt(i)))
				sb.append(Character.toLowerCase(s.charAt(i)));
			else sb.append(Character.toUpperCase(s.charAt(i)));
		}
		return sb;
		
	});
	// executing above lambda expression and printing result
	System.out.println(ob.toggle(obj.getS()));
}
}
