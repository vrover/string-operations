import static java.lang.System.out;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

//functional interface to carry out the operation
@FunctionalInterface
interface Operation4{
	// function that holds the logic
	void split(String s);
	
}


public class Question4 {
private String s;
	public Question4(String s) {
	this.s = s;
}
	public String getS() {
	return s;
}
	public static void main(String[] args) {
		
		Question4 obj=new Question4("zombies eat men men");
		
		// defining lambda expression
		Operation4 ob= (Operation4)((String s)->{
			                               String[] words=s.split("\\s+");
			                                 ArrayList<String> sortedWords=new ArrayList<String>(Arrays.asList(words));
			                                 Collections.sort(sortedWords);
			                                 sortedWords.forEach(word->out.print(word+" "));
		                                       });
		// executing above lambda expression
		ob.split(obj.getS());

	}

}
