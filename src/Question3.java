import static java.lang.System.*;

//functional interface to carry out the operation
@FunctionalInterface
interface Operation3{
	StringBuilder split(String s);
	
}
public class Question3 {
private String s;
	public Question3(String s) {
	this.s = s;
}
	public String getS() {
	return s;
}
	public static void main(String[] args) {
		Question3 obj=new Question3("I love my India");
		
		// defining lambda expression
		Operation3 ob= (Operation3)((String s)->{
			                                 String[] words=s.split("\\s+");
			                                 StringBuilder sb=new StringBuilder(words[0]);
			                                 for(String word:words) {if(word.length()<sb.length())
			                                	                      sb.replace(0, sb.length(), word);
			                                                           }
			                                 return sb;
		                                       });
		
		//printing the result after executing above expression
		out.print(ob.split(obj.getS()));

	}

}
