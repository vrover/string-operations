import java.util.Arrays;

public class Question9 {
private String s;

public Question9(String s) {
	super();
	this.s = s.replaceAll("\\s", "");
}

public String getS() {
	return s;
}

//function that prints characters at odd positions
public void operation(String s) {
	char characters[]=s.toCharArray();
	Arrays.sort(characters);
	int i=0;
	StringBuilder sb=new StringBuilder();
	while(i<characters.length) {
		sb.append(characters[i]);
		i=i+2;
	}
	
	System.out.print(sb.toString());
}
public static void main(String[] args) {
	Question9 obj=new Question9("varshan ttg");
	
	//calling function
	obj.operation(obj.getS());
}

}
